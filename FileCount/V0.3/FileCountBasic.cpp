/*
* 文件名：FileCountBasic.cpp
* 作者：严隽铭
* 描述：统计纯英文txt文本中的字符数、单词数、句子数、代码行、空行、注释行
* 修改人：严隽铭
* 修改时间：2022-11-1
* 修改内容：
*/

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

using namespace std;

/*
* FileCountBasic类集中控制对文本字符等统计的操作。
* 全部为静态变量和静态方法，对外提供统一接口。
* @author 严隽铭
* @version 0.2,2022-11-1
* @see 
* @since 
* @deprecated
*/
class FileCountBasic
{

public:
    FILE* fi;
    char filename[300];//文件名
    unsigned char ar,choice;//ar--文本每个字符内容；choice--模式选择
    unsigned int num[7];//存储字符、单词、句子个数等
    void FileInit(char* filename);
    void FileChar(void);
    void FileVocabulary(void);
    void FileSentence(void);
    void FileExegesis(void);
    void FileLine(void);
    void FileBlackLine(void);
    void FileCountMode(unsigned char choice);
};

//文件初始化
void FileCountBasic::FileInit(char* filename)
{
    fi=fopen(filename,"r");//尝试打开filename并打开文件指针
    if(fi==NULL)//若是没有则跳出
    {
        cout<<"NO FILE!"<<endl;
        exit(0);
    }
}

//统计字符个数
void FileCountBasic::FileChar(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);
            if((ar!='\r')&&(ar!='\n'))
            {
                num[0]++;
            }
        }
    }
}

//统计单词个数
void FileCountBasic::FileVocabulary(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if(((ar>='a')&&(ar<='z'))||((ar>='A')&&(ar<='Z')))
            {
                while(((ar>='a')&&(ar<='z'))||((ar>='A')&&(ar<='Z')))
                {
                    ar=getc(fi);
                }   
                num[1]++;             
            }
        }
    }
    fclose(fi);
}

//统计句子个数
void FileCountBasic::FileSentence(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if((ar=='.')||(ar=='!')||(ar=='?'))
            {
                num[2]++;
            }
        }
    }  
    fclose(fi);  
}

//统计注释行个数
void FileCountBasic::FileExegesis(void)
{
    unsigned char com=0;//两种注释模式切换
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if(com==0)//行注释
            {
                if(ar=='/')
                {
                    ar=getc(fi);
                    if(ar=='*')
                    {
                        num[3]++;com=1;  
                    }
                    else if(ar=='/')
                    {
                        num[3]++;
                    }
                }                
            }
            else if(com==1)//块注释
            {
                if(ar=='\n')
                    num[3]++;
                if(ar=='/')
                    com=0;
            }
        }
    }  
    fclose(fi);    
}

//统计文本行数
void FileCountBasic::FileLine(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if(ar=='\n')
            {
                num[4]++;
            }
        }
    }  
    fclose(fi);     
}

//统计空白行数
void FileCountBasic::FileBlackLine(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if(ar=='\n')
            {
                ar=getc(fi);//指针移至下一位
                if(ar=='\n')
                {
                    num[5]++;
                }            
            }

        }
    }  
    fclose(fi);     
}

//文本统计模式选择
void FileCountBasic::FileCountMode(unsigned char choice)
{
    //c--统计字符个数
    if(choice=='c')
    {
        FileChar();
        cout<<endl;
        cout<<"文本中的字符串个数为"<<num[0]<<endl;
    }

    //v--统计单词个数
    else if(choice=='v')
    {
        FileVocabulary();
        cout<<endl;
        cout<<"文本中的单词个数为"<<num[1]<<endl;
    }

    //s-统计句子个数
    else if(choice=='s')
    {
        FileSentence();
        cout<<endl;
        cout<<"文本中的句子个数为"<<num[2]<<endl;
    }

    //e--统计注释个数
    else if(choice=='e')
    {
        FileExegesis();
        cout<<endl;
        cout<<"文本中的注释个数为"<<num[3]<<endl;
    }

    //l--统计代码行数个数
    else if(choice=='l')
    {
        FileLine();
        cout<<endl;
        cout<<"文本中的代码行数个数为"<<num[4]+1<<endl;
    }

    //b-统计句子个数
    else if(choice=='b')
    {
        FileBlackLine();
        cout<<endl;
        cout<<"文本中的代码空白行个数为"<<num[5]<<endl;
    }

}


int main()
{
    FileCountBasic file;
    while(1)
    {      
        //存储字符等信息清零
        file.num[0]=0;file.num[1]=0;file.num[2]=0;
        file.num[3]=0;file.num[4]=0;file.num[5]=0;

        //显示信息
        cout<<"enter c to count the file char"<<endl;
        cout<<"enter v to count the file vocabulary"<<endl;
        cout<<"enter s to count the file sentence"<<endl;
        cout<<"enter e to count the file exegesis"<<endl;
        cout<<"enter l to count the file line"<<endl;
        cout<<"enter b to count the file blackline"<<endl;        
        cout<<"wc.exe -";

        //输入信息
        cin>>file.choice;
        if((file.choice !='c')&&(file.choice !='v')&&(file.choice !='s')&&(file.choice !='e')&&(file.choice !='l')&&(file.choice !='b'))
        {
            cout<<"enter false,please enter again"<<endl;
            cout<<"wc.exe -";
            cin>>file.choice;
        }
        cin>>file.filename;

        //输出文本字符等信息
        file.FileInit(file.filename);
        file.FileCountMode(file.choice);
    }
    return 0;
}