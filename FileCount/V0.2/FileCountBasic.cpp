/*
* 文件名：FileCountBasic.cpp
* 作者：严隽铭
* 描述：统计纯英文txt文本中的字符数，单词数，句子数
* 修改人：严隽铭
* 修改时间：2022-11-1
* 修改内容：
*/

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

using namespace std;

/*
* FileCountBasic类集中控制对文本字符等统计的操作。
* 全部为静态变量和静态方法，对外提供统一接口。
* @author 严隽铭
* @version 0.2,2022-11-1
* @see 
* @since 
* @deprecated
*/
class FileCountBasic
{

public:
    FILE* fi;
    char filename[300];//文件名
    unsigned char ar,choice;//ar--文本每个字符内容；choice--模式选择
    unsigned int num[4];//存储字符、单词、句子个数
    void FileInit(char* filename);
    void FileChar(void);
    void FileVocabulary(void);
    void FileSentence(void);
    void FileCountMode(unsigned char choice);
};

//文件初始化
void FileCountBasic::FileInit(char* filename)
{
    fi=fopen(filename,"r");//尝试打开filename并打开文件指针
    if(fi==NULL)//若是没有则跳出
    {
        cout<<"NO FILE!"<<endl;
        exit(0);
    }
}

//统计字符个数
void FileCountBasic::FileChar(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);
            if((ar!='\r')&&(ar!='\n'))
            {
                num[0]++;
            }
        }
    }
}

//统计单词个数
void FileCountBasic::FileVocabulary(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if(((ar>='a')&&(ar<='z'))||((ar>='A')&&(ar<='Z')))
            {
                while(((ar>='a')&&(ar<='z'))||((ar>='A')&&(ar<='Z')))
                {
                    ar=getc(fi);
                }   
                num[1]++;             
            }
        }
    }
    fclose(fi);
}

//统计句子个数
void FileCountBasic::FileSentence(void)
{
    rewind(fi);//将光标跳回到文件开头
    getc(fi);//指针移至下一位
    if (feof(fi))//排除空文件可能
    {
        printf("文件为空");
    }
    else
    {
        rewind(fi);//将光标跳回到文件开头
        while(!feof(fi))//开始记录字符个数
        {
            ar=getc(fi);//指针移至下一位
            if((ar=='.')||(ar=='!')||(ar=='?'))
            {
                num[2]++;
            }
        }
    }  
    fclose(fi);  
}

//文本统计模式选择
void FileCountBasic::FileCountMode(unsigned char choice)
{
    //c--统计字符个数
    if(choice=='c')
    {
        FileChar();
        cout<<endl;
        cout<<"文本中的字符串个数为"<<num[0]<<endl;
    }

    //v--统计单词个数
    else if(choice=='v')
    {
        FileVocabulary();
        cout<<endl;
        cout<<"文本中的单词个数为"<<num[1]<<endl;
    }

    //s-统计句子个数
    else if(choice=='s')
    {
        FileSentence();
        cout<<endl;
        cout<<"文本中的句子个数为"<<num[2]<<endl;
    }
}


int main()
{
    FileCountBasic file;
    while(1)
    {      
        //存储字符等信息清零
        file.num[0]=0;file.num[1]=0;file.num[2]=0;

        //显示信息
        cout<<"enter c to count the file char"<<endl;
        cout<<"enter v to count the file vocabulary"<<endl;
        cout<<"enter s to count the file sentence"<<endl;
        cout<<"wc.exe -";

        //输入信息
        cin>>file.choice;
        if((file.choice !='c')&&(file.choice !='v')&&(file.choice !='s'))
        {
            cout<<"enter false,please enter again"<<endl;
            cout<<"wc.exe -";
            cin>>file.choice;
        }
        cin>>file.filename;

        //输出文本字符等信息
        file.FileInit(file.filename);
        file.FileCountMode(file.choice);
    }
    return 0;
}